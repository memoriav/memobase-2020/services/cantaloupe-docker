# Cantaloupe on Docker

This repository provides the Dockerfiles for creating Docker images of
[Cantaloupe](https://cantaloupe-project.github.io), a IIIF imageserver.

## Run container

```sh
docker run --name cantaloupe --rm -d -p8182:8182 -v<path_to_host_image_folder>:/home/cantaloupe/images cantaloupe:<version>
```

You need at least to open a port for communication (image delivery and
optionally communication via REST API and admin UI) and a folder where you
keep your media files.

### Helm charts

For the purpose of running Cantaloupe container inside the Kubernetes cluster
used for Memobase, there are some Helm charts available in the `helm-charts/`
directory. If you want to deploy a container to Kubernetes in another context,
these charts can probably be used as a starting point for your own setup.

## Version management

This repository offers Docker images for different master versions of
the Cantaloupe. Additionally, all versions come in a range of "flavors" which
represent some combinations of processors. For a comprehensive list see the
[container
registry](https://gitlab.switch.ch/memoriav/memobase-2020/services/cantaloupe-docker/container_registry/261).

### Cantaloupe versions

In order to build an image out of a specific version, use the
respective branch. At the moment, there are branches for the two latest major
versions of Cantaloupe: `4.x` and `5.x`. This naming convention is the same as
used for the image tags. The `latest` image tag points to the branch with the
latest stable major version of Cantaloupe, that means as of today to `5.x`.
Duly updated, the branches use the latest minor versions of the respective
major version of Cantaloupe:

- `4.x` -> `4.1.9`
- `5.x.` -> `5.0.4`

### Processor versions

Available processors are `ffmpeg` (for video), `kakadu`, `grok` and `openjpeg`
for JPEG2000 (the former proprietary, the latters free software) and
`turbojpeg`. Readily available flavors:

| name                           | ffmpeg   | turbojpeg | grok     | kakadu     | openjpeg |
| ---                            | :---:    | :---:     | :---:    | :---:      | :---:    |
| `<version>-all-kakadu`         | &#10003; | &#10003;  | &#215;   | &#10003;   | &#215;   |
| `<version>-all-openjpeg`       | &#10003; | &#10003;  | &#215;   | &#215;     | &#10003; |
| `<version>-all-grok`           | &#10003; | &#10003;  | &#10003; | &#215;     | &#215;   |
| `<version>-kakadu-ffmpeg`      | &#10003; | &#215;    | &#215;   | &#10003;   | &#215;   |
| `<version>-kakadu-turbojpeg`   | &#215;   | &#10003;  | &#215;   | &#10003;   | &#215;   |
| `<version>-kakadu`             | &#215;   | &#215;    | &#215;   | &#10003;   | &#215;   |
| `<version>-openjpeg-ffmpeg`    | &#10003; | &#215;    | &#215;   | &#215;     | &#10003; |
| `<version>-openjpeg-turbojpeg` | &#215;   | &#10003;  | &#215;   | &#215;     | &#10003; |
| `<version>-openjpeg`           | &#215;   | &#215;    | &#215;   | &#215;     | &#10003; |
| `<version>-grok-ffmpeg`        | &#10003; | &#215;    | &#10003; | &#215;     | &#215;   |
| `<version>-grok-turbojpeg`     | &#215;   | &#10003;  | &#10003; | &#215;     | &#215;   |
| `<version>-grok`               | &#215;   | &#215;    | &#10003; | &#215;     | &#215;   |
| `<version>-turbojpeg-ffmpeg`   | &#10003; | &#10003;  | &#215;   | &#215;     | &#215;   |
| `<version>-turbojpeg`          | &#215;   | &#10003;  | &#215;   | &#215;     | &#215;   |
| `<version>-ffmpeg`             | &#10003; | &#215;    | &#215;   | &#215;     | &#215;   |
| `<version>-no-deps`            | &#215;   | &#215;    | &#215;   | &#215;     | &#215;   |


## Configuration

You have different options to change the default configuration of Cantaloupe.
Notice that some changes can affect the overall working of the container
(e.g. changing the port used by Cantaloupe)

1. Change the `cantaloupe.properties` file in this directory and rebuild the
   image
2. Mount your own `cantaloupe.properties` file on container startup by
   providing the argument
`-v<path_to_your_config>:/home/cantaloupe/app/cantaloupe.properties`
3. Selectively override properties by setting appropriate environment
   variables on container startup (`-e<key>:<value>). The key translates to
uppercase letters, `.` being replaced by `_` (e.g. `https.key_store_path` ->
`HTTPS_KEY_STORE_PATH`).

### Example: Use with Redis

Cantaloupe supports [Redis](https://redis.io) as an external key-value store for caching.

```sh
docker network create cantaloupe
docker run -d --name redis \
  --restart on-failure \
  --network cantaloupe \
  redis:5-buster
docker run -d --name cantaloupe \
  --rm -p 8182:8182 \
  --network cantaloupe \
  -eCACHE_SERVER_DERIVATIVE_ENABLED=true \
  -eCACHE_SERVER_DERIVATIVE=RedisCache \
  -eREDISCACHE_HOST=redis \
  -v<path_to_host_image_folder>:/home/cantaloupe/images \
  cantaloupe:4.x
```

## Hacking Docker Images

Because of the relatively wide variety of possible Docker images, the `Dockerimage` as basis for the build is generated dynamically during the CI/CD process. A small executable `generate-dockerfile` in the root directory helps to produce the target Dockerfile out of a [template](./Dockerfile.template).

You have different options to hack the Docker image generation:

* Call the executable with custom parameters. See [`.gitlab-ci.yml`](./gitlab-ci.yml) for a few ideas.
* Change the template: The template uses a few simple statements from the [tera template language](https://tera.netlify.app/).
* Adapt the executable directly. You can find the source code in the directory [`generate-dockerfile-src`](./generate-dockerfile-src).
