class CustomDelegate
  attr_accessor :context

  def deserialize_meta_identifier(meta_identifier)
  end

  def pre_authorize(options = {})
    true
  end

  def authorize(options = {})
    true
  end

  def extra_iiif2_information_response_keys(options = {})
    {}
  end

  def extra_iiif3_information_response_keys(options = {})
    {}
  end

  def azurestoragesource_blob_key(options = {})
  end

  def httpsource_resource_info(option = {})
    context.each{ |k,v| puts "#{k}: #{v}" }
    return context['identifier'].gsub('$','/').gsub('"', '%22')
  end

  def jdbcsource_database_identifier(options = {})
  end

  def jdbcsource_media_type(options = {})
  end

  def jdbcsource_lookup_sql(options = {})
  end

  def s3source_object_info(options = {})
  end

  def overlay(options = {})
  end

  def redactions(options = {})
    []
  end

  def metadata(options = {})
  end

  def source(options = {})
    context.each{ |k,v| puts "#{k}: #{v}" }
    uri = context['identifier']
    if uri.start_with?("http") 
      return 'HttpSource'
    else
      return 'FilesystemSource'
    end
  end

 def filesystemsource_pathname(options = {})
    context.each{ |k,v| puts "#{k}: #{v}" }
    filename = context['identifier']
    if filename.start_with?("file:")
      filename = filename.split("$").last
    end
    return "/home/cantaloupe/images/#{filename}"
 end
end
