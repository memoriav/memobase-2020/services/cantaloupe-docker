use lazy_static::lazy_static;
use serde::Serialize;
use std::env;
use std::fs;
use std::process;
use tera::{Context, Result, Tera};

#[derive(Serialize)]
struct TmpltArgs {
    ffmpeg: bool,
    grok: bool,
    kakadu: bool,
    openjpeg: bool,
    turbojpeg: bool,
    version: String,
    port: String,
}

fn main() -> Result<()> {
    lazy_static! {
        pub static ref TEMPLATE: Tera = {
            match Tera::new("*.template") {
                Ok(t) => t,
                Err(e) => {
                    eprintln!("Error occurred: {}", &e);
                    process::exit(1);
                }
            }
        };
    };

    let mut tmplt_args = TmpltArgs {
        ffmpeg: false,
        grok: false,
        kakadu: false,
        openjpeg: false,
        turbojpeg: false,
        version: String::with_capacity(5),
        port: String::with_capacity(5),
    };

    for arg in env::args().skip(1) {
        if arg == "--ffmpeg" {
            tmplt_args.ffmpeg = true;
        } else if arg == "--grok" {
            tmplt_args.grok = true;
        } else if arg == "--kakadu" {
            tmplt_args.kakadu = true;
        } else if arg == "--openjpeg" {
            tmplt_args.openjpeg = true;
        } else if arg == "--turbojpeg" {
            tmplt_args.turbojpeg = true;
        } else if arg.starts_with("--version=") {
            tmplt_args.version = String::from(arg.split("=").nth(1).unwrap());
        } else if arg.starts_with("--port=") {
            tmplt_args.port = String::from(arg.split("=").nth(1).unwrap());
        }
    }

    if tmplt_args.version.is_empty() {
        eprintln!("Please declare Cantaloupe version with `--version=x.y.z`");
        process::exit(1);
    }
    if tmplt_args.port.is_empty() {
        eprintln!("Please declare Cantaloupe port with `--port=xxxxx`");
        process::exit(1);
    }

    let docker_image = TEMPLATE.render(
        "Dockerfile.template",
        &Context::from_serialize(&tmplt_args)?,
    )?;
    fs::write("Dockerfile", docker_image)?;
    Ok(())
}
